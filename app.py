import json
import redis
from datetime import datetime
from requests_futures.sessions import FuturesSession
from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/compare', methods=['POST'])
def compare():
	cache = redis.Redis(host='redis', port=6379, db=0)
	city = request.form['city']
	check_in = request.form['check_in']
	check_out = request.form['check_out']
	reset_cache = '0'
	cached_copy = None

	if 'reset_cache' in request.form:
		reset_cache = request.form['reset_cache']

	cache_key = f'{city.strip()}_{check_in.strip()}_{check_out.strip()}'
	cached_copy = cache.get(cache_key)

	if cached_copy and reset_cache == '0':
		cached_time = cache.get('cache_time')
		return render_template(
			'compare.html',
			data=json.loads(cached_copy),
			check_in=check_in,
			check_out=check_out,
			city=city,
			cache_time=cached_time.decode()
		)

	api_url = 'https://experimentation.snaptravel.com/interview/hotels'
	payload = {
		'city': city,
		'checkin': check_in,
		'checkout': check_out,
	}
	snaptravel_payload = {**payload, **{'provider': 'snaptravel'}}
	retail_payload = {**payload, **{'provider': 'retail'}}

	# Makes Async Requests Using Threads Underneath
	session = FuturesSession()
	snap_api = session.post(api_url, json=snaptravel_payload)
	retail_api = session.post(api_url, json=retail_payload)
	snap_hotels = snap_api.result().json().get('hotels')
	retail_hotels = retail_api.result().json().get('hotels')

	merger = []

	'''
	Holds a map of SnapTravel hotel ids and a full representation of the data. This makes it easier to search in the next
	steps.
	The runtime of this will be a fixed 2 * O(N)
	The first O(N) will be to build this map, the second O(N) occurs when the retail_hotels array is looped - line 46.
	
	This still gives better performance than running nested for loops which will be O(N^2)
	'''
	id_maps = {hotel['id']: hotel for hotel in snap_hotels}

	for hotel in retail_hotels:
		if hotel['id'] in id_maps:
			merger.append({
				**hotel,
				**{
					'snaptravel_id': id_maps[hotel['id']]['id'],
					'snaptravel_price': id_maps[hotel['id']]['price'],
					'hotel.com_price': hotel['price'], 
					'amenities': ', '.join(hotel['amenities'])
				}
			})

	cache.set(cache_key, json.dumps(merger))
	now = datetime.now().strftime("%Y-%m-%d %H:%I:%S")
	cache.set('cache_time', now)
	return render_template('compare.html', data=merger, check_in=check_in, check_out=check_out, city=city, cache_time=now)



if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)