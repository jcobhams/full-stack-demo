FROM python:3.7-stretch

RUN apt-get update && apt-get install gcc

EXPOSE 5000
# Scaffold App
RUN mkdir /app
COPY requirements.txt /app
WORKDIR /app
RUN rm -rf /root/.pip/
RUN pip install -r requirements.txt

ADD . /app

CMD ["python", "app.py", "runserver"]